const config = {
  development: {
    backendUrl: "https://shirt3js.onrender.com/api/v1/dalle",
  },
  production: {
    backendUrl: "https://devswag.onrender.com/api/v1/dalle",
  },
};

export default config;
